﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameListController : MonoBehaviour
{

    private int iTabSelected = 0;
    public void OnGUI()
    {
        GUILayout.BeginVertical();
        {
            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Toggle(iTabSelected == 0, "Tab1", EditorStyles.toolbarButton))
                    iTabSelected = 0;

                if (GUILayout.Toggle(iTabSelected == 1, "Tab2", EditorStyles.toolbarButton))
                    iTabSelected = 1;
            }
            GUILayout.EndHorizontal();

            //Draw different UI according to iTabSelected
            DoGUI(iTabSelected);

        }
        GUILayout.EndVertical();
    }

    void Start()
    {
        
    }


    // Update is called once per frame
    
    void Update()
    {
        
    }

    private void DoGUI(int iTabSelected)
    {
        if (iTabSelected == 0)
        {
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            {
                GUILayout.Button("Tab1.Button1");
                GUILayout.Button("Tab1.Button2");
            }
            GUILayout.EndVertical();
        }

        if (iTabSelected == 1)
        {
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            {
                GUILayout.Label("Tab2.Label1");
                GUILayout.Label("Tab2.Label2");
            }

            GUILayout.EndVertical();
        }
    }
}
